import React, { Component } from "react";
import Chess from "./Chess.js";
import { connect } from "react-redux";
import { updateGameAction } from "../redux/actions.js";
import Chessboard from "chessboardjsx";

class HumanVsHuman extends Component {
  componentDidMount() {
    var game = new Chess();
    this.props.updateGame({ game: game });
  }

  // keep clicked square style and remove hint squares
  removeHighlightSquare = () => {
    var pieceSquare = this.props.pieceSquare;
    var history = this.props.history;
    this.props.updateGame({
      squareStyles: squareStyling({ pieceSquare, history }),
    });
    // this.setState(({ pieceSquare, history }) => ({
    //   squareStyles: squareStyling({ pieceSquare, history }),
    // }));
  };

  // show possible moves
  highlightSquare = (sourceSquare, squaresToHighlight) => {
    const highlightStyles = [...squaresToHighlight].reduce((a, c) => {
      return {
        ...a,
        ...{
          [c]: {
            background:
              "radial-gradient(circle, #00000047 36%, transparent 40%)",
            borderRadius: "50%",
          },
        },
        ...squareStyling({
          history: this.props.history,
          pieceSquare: this.props.pieceSquare,
        }),
      };
    }, {});

    var squareStyles = this.props.squareStyles;
    this.props.updateGame({
      squareStyles: { ...squareStyles, ...highlightStyles },
    });

    // this.setState(({ squareStyles }) => ({
    //   squareStyles: { ...squareStyles, ...highlightStyles },
    // }));
  };

  onDrop = ({ sourceSquare, targetSquare }) => {
    // see if the move is legal
    let move = this.props.game.move({
      from: sourceSquare,
      to: targetSquare,
      promotion: "q", // always promote to a queen for example simplicity
    });

    // illegal move

    if (move === null) return;

    var { pieceSquare, history } = this.props;

    this.props.updateGame({
      fen: this.props.game.fen(),
      history: this.props.game.history({ verbose: true }),
      squareStyles: squareStyling({ pieceSquare, history }),
    });
    // this.setState(({ history, pieceSquare }) => ({
    //   fen: this.props.game.fen(),
    //   history: this.props.game.history({ verbose: true }),
    //   squareStyles: squareStyling({ pieceSquare, history }),
    // }));
  };

  onMouseOverSquare = (square) => {
    // get list of possible moves for this square
    let moves = this.props.game.moves({
      square: square,
      verbose: true,
    });

    // exit if there are no moves available for this square
    if (moves.length === 0) return;

    let squaresToHighlight = [];
    for (var i = 0; i < moves.length; i++) {
      squaresToHighlight.push(moves[i].to);
    }

    this.highlightSquare(square, squaresToHighlight);
  };

  onMouseOutSquare = (square) => this.removeHighlightSquare(square);

  // central squares get diff dropSquareStyles
  onDragOverSquare = (square) => {
    let newVal =
      square === "e4" || square === "d4" || square === "e5" || square === "d5"
        ? { backgroundColor: "cornFlowerBlue" }
        : { boxShadow: "inset 0 0 1px 4px rgb(255, 255, 0)" };
    this.props.updateGame(newVal);
    // this.setState({
    //   dropSquareStyle:
    //     square === "e4" || square === "d4" || square === "e5" || square === "d5"
    //       ? { backgroundColor: "cornFlowerBlue" }
    //       : { boxShadow: "inset 0 0 1px 4px rgb(255, 255, 0)" },
    // });
  };

  onSquareClick = (square) => {
    var { history } = this.props;
    this.props.updateGame({
      squareStyles: squareStyling({ pieceSquare: square, history }),
      pieceSquare: square,
    });
    // this.setState(({ history }) => ({
    //   squareStyles: squareStyling({ pieceSquare: square, history }),
    //   pieceSquare: square,
    // }));

    let move = this.props.game.move({
      from: this.props.pieceSquare,
      to: square,
      promotion: "q", // always promote to a queen for example simplicity
    });

    // illegal move

    if (move === null) return;
    this.props.updateGame({
      fen: this.props.game.fen(),
      history: this.props.game.history({ verbose: true }),
      pieceSquare: "",
    });

    // this.setState({
    //   fen: this.props.game.fen(),
    //   history: this.props.game.history({ verbose: true }),
    //   pieceSquare: "",
    // });
  };

  onSquareRightClick = (square) =>
    this.props.updateGame({
      squareStyles: { [square]: { backgroundColor: "deepPink" } },
    });
  // this.setState({
  //   squareStyles: { [square]: { backgroundColor: "deepPink" } },
  // });

  render() {
    const { fen, dropSquareStyle, squareStyles } = this.props;

    return (
      <Chessboard
        id="humanVsHuman"
        width={620}
        position={fen}
        onDrop={this.onDrop}
        onMouseOverSquare={this.onMouseOverSquare}
        onMouseOutSquare={this.onMouseOutSquare}
        boardStyle={{
          borderRadius: "5px",
          boxShadow: `0 5px 15px rgba(0, 0, 0, 0.5)`,
        }}
        squareStyles={squareStyles}
        dropSquareStyle={dropSquareStyle}
        onDragOverSquare={this.onDragOverSquare}
        onSquareClick={this.onSquareClick}
        onSquareRightClick={this.onSquareRightClick}
      />
    );

    // return this.props.children({
    //   squareStyles,
    //   position: fen,
    //   onMouseOverSquare: this.onMouseOverSquare,
    //   onMouseOutSquare: this.onMouseOutSquare,
    //   onDrop: this.onDrop,
    //   dropSquareStyle,
    //   onDragOverSquare: this.onDragOverSquare,
    //   onSquareClick: this.onSquareClick,
    //   onSquareRightClick: this.onSquareRightClick,
    // });
  }
}

const squareStyling = ({ pieceSquare, history }) => {
  const sourceSquare = history.length && history[history.length - 1].from;
  const targetSquare = history.length && history[history.length - 1].to;

  return {
    [pieceSquare]: { backgroundColor: "rgba(255, 255, 0, 0.4)" },
    ...(history.length && {
      [sourceSquare]: {
        backgroundColor: "rgba(255, 255, 0, 0.4)",
      },
    }),
    ...(history.length && {
      [targetSquare]: {
        backgroundColor: "rgba(255, 255, 0, 0.4)",
      },
    }),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateGame: (payload) => dispatch(updateGameAction(payload)),
  };
};

const mapStateToProps = (state) => {
  return { ...state.chessGame };
};

export default connect(mapStateToProps, mapDispatchToProps)(HumanVsHuman);
