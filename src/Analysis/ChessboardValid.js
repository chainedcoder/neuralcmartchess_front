import React, { Component } from "react";
import Chess from "./Chess.js";
import { connect } from "react-redux";
import Chessboard from "chessboardjsx";
import { updateGameAction } from "../redux/actions.js";
import HumanVsHuman from "./HumanVsHuman";

const ChessboardValid = () => {
  return (
    <div>
      <HumanVsHuman>
        {({
          position,
          onDrop,
          onMouseOverSquare,
          onMouseOutSquare,
          squareStyles,
          dropSquareStyle,
          onDragOverSquare,
          onSquareClick,
          onSquareRightClick,
        }) => (
          <Chessboard
            id="humanVsHuman"
            width={620}
            position={position}
            onDrop={onDrop}
            onMouseOverSquare={onMouseOverSquare}
            onMouseOutSquare={onMouseOutSquare}
            boardStyle={{
              borderRadius: "5px",
              boxShadow: `0 5px 15px rgba(0, 0, 0, 0.5)`,
            }}
            squareStyles={squareStyles}
            dropSquareStyle={dropSquareStyle}
            onDragOverSquare={onDragOverSquare}
            onSquareClick={onSquareClick}
            onSquareRightClick={onSquareRightClick}
          />
        )}
      </HumanVsHuman>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updateGame: (payload) => dispatch(updateGameAction(payload)),
  };
};

const mapStateToProps = (state) => {
  return { ...state.chessGame };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChessboardValid);
