export const chessGame = (
  state = {
    fen: "start",
    // square styles for active drop square
    dropSquareStyle: {},
    // custom square styles
    squareStyles: {},
    // square with the currently clicked piece
    pieceSquare: "",
    // currently clicked square
    square: "",
    // array of past game moves
    history: [],
    uploadedPgn: "",
    loadPgn: false,
  },
  action
) => {
  switch (action.type) {
    case "UPDATE_GAME":
      const payload = action.payload;
      console.log("***", state, payload);
      return {
        ...state,
        ...payload,
      };

    default:
      return state;
  }
};
