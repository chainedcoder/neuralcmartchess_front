export const updateGameAction = (payload) => {
  return {
    type: "UPDATE_GAME",
    payload,
  };
};
