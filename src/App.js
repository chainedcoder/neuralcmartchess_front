import React from "react";
import logo from "./logo.svg";
import "./App.less";
import { Button, Row, Col, Input } from "antd";
import ChessboardValid from "./Analysis/ChessboardValid";
import { connect } from "react-redux";
import { updateGameAction } from "./redux/actions.js";
import HumanVsHuman from "./Analysis/HumanVsHuman";
import {
  FastBackwardOutlined,
  BackwardOutlined,
  FastForwardOutlined,
  ForwardOutlined,
} from "@ant-design/icons";

const { TextArea } = Input;

function App(props) {
  const onImportClick = () => {
    // if (props.uploadedPgn) props.updateGame({ loadPgn: true });
    if (props.uploadedPgn) {
      // console.log("*******", props.uploadedPgn);
      let pgn = props.uploadedPgn;
      props.game.load_pgn(pgn);
      props.updateGame({
        fen: props.game.fen(),
        loadPgn: false,
        uploadedPgn: "",
        history: props.game.history({ verbose: true }),
      });
    }
  };

  const onPgnText = ({ target: { value } }) => {
    props.updateGame({ uploadedPgn: value });
  };

  return (
    <div className="App">
      <div className="board-area">
        <Row>
          <Col span={15}>
            <HumanVsHuman sparePieces={true} />
          </Col>
          <Col className="analysis-area" span={8}>
            <TextArea
              rows={4}
              allowClear
              onChange={onPgnText}
              onPressEnter={onPgnText}
            />
            <Button onClick={onImportClick} type="primary">
              Import PGN
            </Button>
            <div className="moves-list">
              
              <div className="game-nav-buttons">
                <Row>
                  <Col>
                    <Button
                      type="default"
                      size="small"
                      shape="round"
                      icon={<FastBackwardOutlined />}
                    ></Button>
                  </Col>
                  <Col>
                    <Button
                      type="default"
                      size="small"
                      shape="round"
                      icon={<BackwardOutlined />}
                    ></Button>
                  </Col>
                  <Col>
                    <Button
                      type="default"
                      size="small"
                      shape="round"
                      icon={<ForwardOutlined />}
                    ></Button>
                  </Col>
                  <Col>
                    <Button
                      type="default"
                      size="small"
                      shape="round"
                      icon={<FastForwardOutlined />}
                    ></Button>
                  </Col>
                </Row>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateGame: (payload) => dispatch(updateGameAction(payload)),
  };
};

const mapStateToProps = (state) => {
  return { ...state.chessGame };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
